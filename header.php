<?php 
	
	$fonts = "verdana";
	$bgcolor = "#444";
	$fontcolor = "#fff";

 ?>
 <!DOCTYPE html>
 <html>
 	<head>
 		<title>PHP Fundamentals</title>
 		<style type="text/css">
 			body{font-family: <?php echo $fonts; ?>}.phpcoding{width: 900px; margin: 0 auto; background:<?php echo "#ddd" ?>;}.headeroption, .footeroption{background: <?php echo $bgcolor ?>;color: <?php echo $fontcolor; ?>;text-align: center;padding: 20px;}.headeroption h2, .footerption h2{margin: 0;} .maincontent{min-height: 400px; padding: 20px;} p{margin: 0}
 		</style>
 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
 	</head>

 	<body>
 		<div class="phpcoding">
 			<section class="headeroption">
 				<h2><?php echo "PHP Fundamentals Tutorial"; ?></h2>
 			</section>